package com.flashingstrob;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.os.Environment;

public class ExtPathBuilder {
	private static final String ANDROID_FOLDER = "Android";
	private static final String DATA_FOLDER = "data";
	private static final String SLASH = "/";

	private Context mContext;
	private List<String> mFolders;
	private boolean mOnSDCard;

	public ExtPathBuilder(Context context, boolean useSD) {
		mContext = context;
		mFolders = new ArrayList<String>();
		mOnSDCard = useSD;
		if (mOnSDCard) {
			mFolders.add(ANDROID_FOLDER);
			mFolders.add(DATA_FOLDER);
			mFolders.add(mContext.getPackageName());
		}
	}

	public String createStringPath() {
		StringBuilder sb = new StringBuilder();
		if (mOnSDCard) {
			sb.append(Environment.getExternalStorageDirectory().getAbsolutePath()).append(SLASH);
		}
		int count = mFolders.size();
		for (int i = 0; i < count; i++) {
			sb.append(mFolders.get(i)).append(SLASH);
		}
		return sb.toString();
	}

	public void addFolder(String folder) {
		mFolders.add(folder);
	}

	public static void createPathIfNeed(String path) {
		try {
			if (!(new File(path).exists())) {
				new File(path).mkdirs();
			}
		} catch (Exception e) {
		}
	}

	public void clear() {
		mFolders.clear();
	}
}

