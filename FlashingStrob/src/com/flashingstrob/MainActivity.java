package com.flashingstrob;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.flashingstrob.FlashBlinkService.LocalBinder;

public class MainActivity extends Activity implements OnClickListener, OnSeekBarChangeListener {

	private static final String KEY_INSTANCE_BINDER = "KEY_INSTANCE_BINDER";
	public static final String KEY_EXTRA_FREQUENCY = "KEY_EXTRA_FREQUENCY";
	public static final String KEY_EXTRA_BALANCE = "KEY_EXTRA_BALANCE";
	String mNumber1;
	String mNumber2;
	TextView mFrequency;
	private int mFrequencyInt;
	private int mBalanceInt;
	private ServiceConnection mConnection;
	private FlashBlinkService mService;
	static public LocalBinder mBinder;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);
		mFrequency = (TextView) findViewById(R.id.frequency_textview);
		mNumber1 = "2";
		mNumber2 = "3";
		mBalanceInt = 6;
		mFrequency.setText(mNumber1 + mNumber2);
		
		((Button) findViewById(R.id.button0)).setOnClickListener(this);
		((Button) findViewById(R.id.button1)).setOnClickListener(this);
		((Button) findViewById(R.id.button2)).setOnClickListener(this);
		((Button) findViewById(R.id.button3)).setOnClickListener(this);
		((Button) findViewById(R.id.button4)).setOnClickListener(this);

		((Button) findViewById(R.id.button5)).setOnClickListener(this);
		((Button) findViewById(R.id.button6)).setOnClickListener(this);
		((Button) findViewById(R.id.button7)).setOnClickListener(this);
		((Button) findViewById(R.id.button8)).setOnClickListener(this);
		((Button) findViewById(R.id.button9)).setOnClickListener(this);

		((Button) findViewById(R.id.button_on)).setOnClickListener(this);
		((Button) findViewById(R.id.button_off)).setOnClickListener(this);
		((SeekBar) findViewById(R.id.seekBar1)).setOnSeekBarChangeListener(this);
		((SeekBar) findViewById(R.id.seekBar1)).setProgress(5);
		Log.d("111", "onCreate savedInstanceState = " + (savedInstanceState == null));
		Log.d("111", "onCreate mBinder = " + (mBinder == null));
		Log.d("111", "onCreate mService = " + (mService == null));
		if (savedInstanceState != null) {
		
		}else{
			mConnection = null;
			mService = null;
			mBinder = null;
		}
		// if (savedInstanceState != null) {
		// mBinder = (LocalBinder) savedInstanceState.getBinder(KEY_INSTANCE_BINDER);
		// if (mBinder != null) {
		// mService = (FlashBlinkService) mBinder.getService();
		// }
		// }
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		//Log.d("111", "onSaveInstanceState mBinder = " + (mBinder == null));
		// if (mBinder != null) {
		// outState.putBinder(KEY_INSTANCE_BINDER, mBinder);
		// }
	}

	@Override
	protected void onResume() {
		//Log.d("111", "onResume mBinder = " + (mBinder == null));
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		 stopFlash();
		mConnection = null;
		mService = null;
		mBinder = null;
	}

	@Override
	public void onClick(View v) {
		Log.d("111", "v.getId() = " + v.getId());
		Log.d("111", "R.id.button0 = " + R.id.button0);
		if (v.getId() == R.id.button0) {
			tryPushNextNumber(v);
		}
		if (v.getId() == R.id.button1) {
			tryPushNextNumber(v);
		}

		if (v.getId() == R.id.button2) {
			tryPushNextNumber(v);
		}

		if (v.getId() == R.id.button3) {
			tryPushNextNumber(v);
		}

		if (v.getId() == R.id.button4) {
			tryPushNextNumber(v);
		}

		if (v.getId() == R.id.button5) {
			tryPushNextNumber(v);
		}
		if (v.getId() == R.id.button6) {
			tryPushNextNumber(v);
		}
		if (v.getId() == R.id.button7) {
			tryPushNextNumber(v);
		}
		if (v.getId() == R.id.button8) {
			tryPushNextNumber(v);
		}
		if (v.getId() == R.id.button9) {
			tryPushNextNumber(v);
		}

		if (v.getId() == R.id.button_on) {
			mFrequencyInt = Integer.parseInt(mNumber1 + mNumber2);
			if (mFrequencyInt != 0) {
				launchFlash();
			}else{
				Toast.makeText(this, "Incorrect frequency!", Toast.LENGTH_LONG).show();
			}
		}
		if (v.getId() == R.id.button_off) {
			if (mConnection != null) {
				stopFlash();
			} else {
				mNumber1 = "0";
				mNumber2 = "0";
				mFrequency.setText(mNumber2);
			}
		}
	}

	private void tryPushNextNumber(View v) {
		if (isNumberLess3()) {
			mNumber1 = mNumber2;
			mNumber2 = ((Button) v).getText().toString();
		} else if (mNumber2.equals("3")) {
			if (v.getId() == R.id.button0) {
				mNumber1 = "3";
				mNumber2 = "0";
			}
		} else {
			mNumber2 = ((Button) v).getText().toString();
		}
		if (mNumber1.equals("0")) {
			mFrequency.setText(mNumber2);
		} else {
			mFrequency.setText(mNumber1 + mNumber2);
		}
	}

	private boolean isNumberLess3() {
		return mNumber2.equals("0") || mNumber2.equals("1") || mNumber2.equals("2");
	}
	
	private void launchFlash() {
		if (mConnection == null) {
			mConnection = new ServiceConnection() {

				public void onServiceConnected(ComponentName className, IBinder binder) {
					mBinder = (LocalBinder) binder;
					if (mBinder != null)
						mService = (FlashBlinkService) mBinder.getService();
				}

				public void onServiceDisconnected(ComponentName className) {
					mService = null;
				}
			};
			mFrequencyInt = Integer.parseInt(mNumber1 + mNumber2);
			Intent intent = new Intent(MainActivity.this, FlashBlinkService.class);
			intent.putExtra(KEY_EXTRA_FREQUENCY, mFrequencyInt);
			intent.putExtra(KEY_EXTRA_BALANCE, mBalanceInt);
			bindService(intent, mConnection, BIND_AUTO_CREATE);
		}
	}

	private void stopFlash() {
		if (mConnection != null) {
			unbindService(mConnection);
		}
		mConnection = null;
	}

	
	@Override
	public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {

	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		mBalanceInt = seekBar.getProgress() + 1;
		if (mService != null) {
			mService.setDurationFrequency((float) 0.0, (float) mBalanceInt);
		}
	}

}
