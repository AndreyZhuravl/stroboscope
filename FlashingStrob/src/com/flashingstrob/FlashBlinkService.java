package com.flashingstrob;

import android.app.IntentService;
import android.content.Intent;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;

public class FlashBlinkService extends IntentService {

	private static Camera cam = null;
	Parameters params;
	private long sleepMS_On = 1;
	private long sleepMS_Off = 1;
	private boolean isLighOn = true;
	private volatile Boolean isStop = false;
	Object mutexBalance = new Object();
	private BlinkingThread mThread;
	LocalBinder mBinder = new LocalBinder();

	int mFrequncy;
	int mBalance;
	int counter = 0;
	long ds;

	public FlashBlinkService(String name) {
		super(name);
	}

	public FlashBlinkService() {
		this("FlashBlinkService");
		ds = System.currentTimeMillis();
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		mFrequncy = intent.getIntExtra(MainActivity.KEY_EXTRA_FREQUENCY, 10);
		mBalance = intent.getIntExtra(MainActivity.KEY_EXTRA_BALANCE, 2);

		float fr = mFrequncy;
		float bl = mBalance;

		setDurationFrequency(fr, bl);
		startFlashBlink();
	}

	public synchronized void setDurationFrequency(float fr, float bl) {
		if (fr == 0.0) {
			fr = mFrequncy;
		}
		synchronized (mutexBalance) {
			sleepMS_On = (long) (1000.0 / fr);
			sleepMS_Off = (long) (((float) sleepMS_On) * bl / 12.0);
			sleepMS_On = sleepMS_On - sleepMS_Off;
		}
	}

	public class LocalBinder extends Binder {
		FlashBlinkService getService() {
			return FlashBlinkService.this;
		}
	}

	@Override
	public IBinder onBind(Intent intent) {
		onHandleIntent(intent);
		return mBinder;
	};

	class BlinkingThread extends Thread {
		private Handler mBlinkingHandler = new Handler();

		Runnable flashBlinkRunnable = new Runnable() {
			public void run() {
				main();
			}
		};

		@Override
		public void run() {
			mBlinkingHandler.postAtTime(flashBlinkRunnable, 0);
		}

		private void main() {
			try {

				flipFlash();
				synchronized (mutexBalance) {
					Thread.sleep(sleepMS_On);
				}

				flipFlash();
				synchronized (mutexBalance) {
					Thread.sleep(sleepMS_Off);
				}
				if (!isStop) {
					mBlinkingHandler.postDelayed(flashBlinkRunnable, 0);
				} else {
					stopCamera(cam);
				}

			} catch (InterruptedException e) {
			}
		}

		private void mainWithoutHandler() {
			try {
				while (!isStop) {
					flipFlash();
					synchronized (mutexBalance) {
						Thread.sleep(sleepMS_On);
					}

					flipFlash();
					synchronized (mutexBalance) {
						Thread.sleep(sleepMS_Off);
					}
				}

			} catch (InterruptedException e) {
			}
		}

		public void stopBlinking() {
			synchronized (isStop) {
				isStop = true;
			}
		}
	}

	private void initCamera() {
		cam = Camera.open();
		params = cam.getParameters();
		cam.startPreview();
	}

	private void flipFlash() {

		if (isLighOn) {
			params.setFlashMode(Parameters.FLASH_MODE_OFF);
			cam.setParameters(params);
			isLighOn = false;
		} else {
			params.setFlashMode(Parameters.FLASH_MODE_TORCH);
			cam.setParameters(params);
			isLighOn = true;
		}
	}

	public void startFlashBlink() {
		initCamera();
		mThread = new BlinkingThread();
		mThread.start();
	}

	public void stopFlashBlink() {
		mThread.stopBlinking();
	}

	private void stopCamera(Camera cam) {
		if (cam != null) {
			cam.stopPreview();
			cam.release();
			cam = null;
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		stopFlashBlink();
	}
}
